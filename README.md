# Template Project

Template Project works as a python package. It contains:

## Requirements
Base and Dev requirements.

## Services
*Postgres* as db.
*service* as the source code.

## Installation
This application is dockerized, to run this microservices run:
```docker-compose up```

*Note: Most used commands are setted in Makefile, making it the entrypoint of the whole application.*

## Author
Cristopher Adasme (cadasmeq@gmail.com)
