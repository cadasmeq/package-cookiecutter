-r base.txt

# Testing
flake8==3.9.2
tox==3.24.3
pytest==6.2.5
pytest-cov==2.12.1
mypy===0.931

# Pre Commit
pre-commit==2.16.0

# Code Formatting
black==21.12b0
isort==5.10.1